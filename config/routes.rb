Rails.application.routes.draw do
  resources :students do 
    get "/check" => "students#check"
  end

  resources :teachers do 
    get "/change_password", to: "teachers#change_password"
  end

  resources :cells do 
    get "/students_personal_details" => 'cells#students_personal_details'
    get "/summary" => 'cells#summary'
  end
  
  resources :instructions
  resources :attendances
  
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  
  root 'cells#index'
  get '/' => 'cells#index'
  get '/login', to: 'sessions#new', as: 'login'
  get '/logout', to: 'sessions#destroy', as: 'logout'
  get '/search', to: 'application#searchByName'
  get "/admin", to: "application#admin"
  get "/cells_all", to: "cells#all"

  # get "/migration/set_end_of_year_result", to: "migration#set_end_of_year_result"
  # get "/migration/create_new_cells", to: "migration#create_new_cells"
  # get "/migration/assign_new_cells", to: "migration#assign_new_cells"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
